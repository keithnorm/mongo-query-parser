var chai = require('chai');
var expect = chai.expect;
var parseAsMongooseQuery = require('../');

describe('Mongoose Query Generator', function() {
  it('generates simple search', function() {
    var query = parseAsMongooseQuery('foo=bar');
    expect(query.find).to.eql({foo: 'bar'});
  });
  
  function generatesCorrectParamsForRelationalQuery(relation) {
    it('generates correct find params for ' + relation + ' query', function() {
      var query = parseAsMongooseQuery('foo[' + relation + ']=10');
      var key = '$' + relation;
      expect(query.find.foo[key]).to.eql(10);
    });
  }
  
  ['lt', 'gt', 'lte', 'gte'].forEach(generatesCorrectParamsForRelationalQuery);
  
  it('handles near query', function() {
    var query = parseAsMongooseQuery('loc[near]=38.8977,-77.0366');
    expect(query.find.loc['$near']).to.eql([38.8977, -77.0366]);
  });
  
  it('handles near query with distance', function() {
    var query = parseAsMongooseQuery('loc[near]=38.8977,-77.0366&loc[maxDistance]=10');
    expect(query.find.loc['$maxDistance']).to.eql(10);
  });
  
  it('handles query with not equal', function() {
    var query = parseAsMongooseQuery('foo[not]=bar');
    expect(query.find.foo.$ne).to.eql(/bar/i);
  });
  
  it('handles sort param', function() {
    var query = parseAsMongooseQuery('sort=foo');
    expect(query.sort.foo).to.eql(1);
  });
  
  it('handles sort param with order', function() {
    var query = parseAsMongooseQuery('sort=foo&sortOrder=-1');
    expect(query.sort.foo).to.eql(-1);
  });
  
  it('handles pagination', function() {
    var query = parseAsMongooseQuery('page=3&perPage=10');
    expect(query.perPage).to.eql(10);
    expect(query.page).to.eql(3);
  });
  
  it('handles nested find', function() {
    var query = parseAsMongooseQuery('foo.bar=baz');
    expect(query.find).to.eql({'foo.bar': 'baz'});
  });
  
  it('handles geoWithin style queries', function() {
    // { <location field> :
    //                          { $geoWithin :
    //                             { $geometry :
    //                                { type : "Polygon" ,
    //                                  coordinates : [ [ [ <lng1>, <lat1> ] , [ <lng2>, <lat2> ] ... ] ]
    //                       } } } }
    // leads.find({ loc: { '$geoWithin': { '$box': [ [ -123.3082518871026, -6.557007406511283 ], [ -68.80090948985047, 65.19327218437229 ] ] } } })
    // leads.find({ loc: { '$geoWithin': { '$box': [ [ -6.557007406511283, 65.19327218437229 ], [ -123.3082518871026, -68.80090948985047 ] ] } } })
    // loc[within][box][sw]=-6.557007406511283,65.19327218437229&loc[within][box][ne]=-123.3082518871026,%20-68.80090948985047
    // db.leads.find({ loc: { '$near': { '$geometry': { type: 'Point', coordinates: [ 10, 10 ]  }, '$maxDistance': 10000000 } } })
    var query = parseAsMongooseQuery('loc[within][box][sw]=-123.3082518871026, -6.557007406511283&loc[within][box][ne]=-68.80090948985047, 65.19327218437229&loc[maxDistance]=10');
    console.log('GOT', query.find.loc.$geoWithin);
    expect(query.find.loc.$geoWithin).to.eql({
      "$box": [[-123.3082518871026, -6.557007406511283],
               [-68.80090948985047, 65.19327218437229]]
            });
      expect(query.find.loc.$maxDistance).to.equal(10);
  });
  
  it('handles nested find with options', function() {
    var query = parseAsMongooseQuery('foo.bar.age[gte]=21');
    expect(query.find).to.eql({'foo.bar.age': {$gte: 21}});
  });
  
  it('handles combination query', function() {
    var query = parseAsMongooseQuery('foo=bar&loc[near]=123,321&page=3&perPage=10&age[gte]=21');
    expect(query).to.eql({
      "find": {
        "age": {
          "$gte": 21
        },
        "foo": "bar",
        "loc": {
          "$near": [
            123,
            321
          ],
        }
      },
      "sort": undefined,
      "page": 3,
      "perPage": 10
    })
  });
  
  it('should not alter the original input', function() {
    var input = {foo: {gte: '21'}};
    parseAsMongooseQuery(input);
    expect(input.foo.gte).to.equal('21');
  });
});