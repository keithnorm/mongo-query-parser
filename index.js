var qs = require('qs');

function deepCopy(obj) {
  var out = {};
  _deepCopy(obj, out);
  return out;
}

function _deepCopy(input, output) {
  for (var key in input) {
    if (typeof input[key] == "object") {
      output[key] = {};
      _deepCopy(input[key], output[key]);
    } else {
      output[key] = input[key];
    }
  }
  return output;
}

function mongoify(query) {
  var params = deepCopy(qs.parse(query));
  
  for (var key in params) {
    processParam(key, params[key], params);
  }
  var sort = params.sort;
  var page = params.page;
  var perPage = params.perPage;
  ['sort', 'perPage', 'page'].forEach(function(key) {
    delete params[key];
  });
  
  return {
    find: params,
    sort: sort,
    page: page,
    perPage: perPage
  };
}

function processParam(key, val, params) {
  if (typeof val === "object") {
    for (var nestedProp in val) {
      processParam(nestedProp, val[nestedProp], val);
    }
  }
  
  switch(key) {
    case 'lt':
    case 'gt':
    case 'lte':
    case 'gte':
      delete params[key];
      params['$' + key] = parseFloat(val);
      break;
      
    case 'near':
      delete params[key];
      var latLng = val.split(',');
      params['$' + key] = [parseFloat(latLng[0]), parseFloat(latLng[1])];
      break;
    
    case 'maxDistance': 
      delete params[key];
      var nearQueryTerms = params.$near;
      
      if (nearQueryTerms) {
        params.$maxDistance = parseFloat(val);
      }
      break;
      
    case 'not':
      delete params[key];
      var neregex = new RegExp(val, "i");
      params.$ne = neregex;
      break;
    
    case 'within':
          // leads.find({ loc: { '$geoWithin': { '$box': [ [ -123.3082518871026, -6.557007406511283 ], [ -68.80090948985047, 65.19327218437229 ] ] } } })
      var box = params[key]['box'];
      delete params[key];
      var sw = box.sw.split(',');
      var ne = box.ne.split(',');
      params.$geoWithin = {
        '$box': [
                  [parseFloat(sw[0]), parseFloat(sw[1])],
                  [parseFloat(ne[0]), parseFloat(ne[1])]
                ]
      }
      if (params.maxDistance) {
        params.$maxDistance = parseFloat(params.maxDistance);
        delete params.maxDistance;
      }
      break;
      
    case 'sort': 
      params.sort = {};
      params.sort[val] = parseInt(params.sortOrder) || 1;
      break;
    
    case 'page':
    case 'perPage':
      params[key] = parseInt(params[key]);
      break;
      
    default:
      params[key] = val;
      break;
   }
}

module.exports = mongoify;